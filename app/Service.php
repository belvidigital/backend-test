<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'name'
    ];

    //======= RELATIONSHIPS //=======
    public function treatments()
    {
        return $this->hasMany('App\Treatment');
    }
}
